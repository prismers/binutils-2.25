#!/usr/bin/env python3.6

import os
import sys
import argparse
import json
import subprocess
import multiprocessing
import shutil

"""
{
    "global_options": {
        "watermark": "the watermark",
        "suite": "academic",
        "installer_filename": "installer_filename", 
        "language": ["c-c++", "java"]
    },
    "components": {
        "PINPOINT": { 
            "repo_name": "pinpoint",
            "commit_git": "db0c2420662aa93b0f3d78bc4e5fee9e22314ab1",
            "install_dir": "/path/to/install_dir",
            "build_dir": "/path/to/build_dir",
            "source_path": "/path/to/source_code_path"
        },

        "LLVM_4_0": {
            "repo_name": "llvm4.0",
            "commit_git": "db0c2420662aa93b0f3d78bc4e5fee9e22314ab1",
            "install_dir": "/path/to/install_dir",
            "build_dir": "/path/to/build_dir",
            "source_path": "/path/to/source_code_path"
        }
    }
}
"""


class Config:
    def __init__(self):
        self._config_path = None
        self.enable_cache_id = False
        self._prefix = None
        return

    def parse_args(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('--load-config', type=str, default=None, help='A file path of a Json configuration file.')
        parser.add_argument('--enable-hash-code', action='store_true',
                            help='A string indicating that which key in config file this script uses.')
        parser.add_argument('--with-package-prefix', type=str, default=None, help='Install path.')
        args = parser.parse_args()
        self._config_path = args.load_config
        self.enable_cache_id = args.enable_hash_code
        self._prefix = args.with_package_prefix
        return

    def get_config_cmd(self) -> list:
        prefix = self.get_install_prefix()
        cmd = [os.path.join(os.path.abspath(os.path.dirname(__file__)), 'configure'), '--enable-gold',
                      '--enable-plugins', '--disable-werror', '--enable-ld=no', '--prefix=%s' % prefix,
                      'CXXFLAGS=-s', 'CFLAGS=-s']
        return cmd

    def get_cache_id(self):
        return ''

    def get_build_path(self):
        with open(self._config_path) as file:
            config = json.load(file)
            build_path = config['components']['BINUTILS_2_25']['build_dir']
        if not os.path.isabs(build_path):
            raise Exception('The build directory of is not an absolute path.')
        return build_path

    def get_install_prefix(self):
        if self._prefix is not None:
            return self._prefix
        with open(self._config_path) as file:
            config = json.load(file)
            install_path = config['components']['BINUTILS_2_25']['install_dir']
        if not os.path.isabs(install_path):
            raise Exception('The install directory is not an absolute path.')
        return install_path


def main():
    config = Config()
    config.parse_args()
    if config.enable_cache_id:
        try:
            print(config.get_cache_id())
        except:
            return -1
    else:
        try:
            cmd = config.get_config_cmd()
        except Exception as e:
            print("Something wrong with Json config file: %s" % str(e), file=sys.stderr)
            return 1

        try:
            build_path = config.get_build_path()
            if os.path.isdir(build_path):
                shutil.rmtree(build_path)
            os.mkdir(build_path)
        except Exception as e:
            print("Can not create build directory: %s" % str(e), file=sys.stderr)
            return 2

        print(cmd)
        try:
            subprocess.check_call(cmd, cwd=build_path)
            subprocess.check_call(['make', '-j' + str(multiprocessing.cpu_count())], cwd=build_path)
        except Exception as e:
            print(str(e), file=sys.stderr)
            return 3

        try:
            subprocess.check_call(['make', 'install'], cwd=build_path)
        except Exception as e:
            print(str(e), file=sys.stderr)
            return 4
    return 0


if __name__ == '__main__':
    sys.exit(main())

